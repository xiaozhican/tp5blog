<?php

namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class Admin extends Model
{
    //软删除
    use SoftDelete;
    //只读字段
    protected $readonly = ['email'];

    //登录效验
    public function login($data)
    {
        $validate = new \app\common\validate\Admin();
        if(!$validate->scene('login')->check($data)){
            return $validate->getError();
        }
        $info = $this->where($data)->find();
        if($info){
            //判断用户是否可用
            if($info['status'] != 1){
                return '此账户被禁用！';
            }
            $sessionData = [
                'id'=>$info['id'],
                'nickname'=>$info['nickname'],
                'is_super'=>$info['is_super']
            ];
            session('admin',$sessionData);
            return 1;
        }else{
            return '用户名或密码错误！';
        }
    }
    //注册
    public function register($data)
    {
        $validate = new \app\common\validate\Admin();
        if(!$validate->scene('register')->check($data)){
            return $validate->getError();
        }
        $res = $this->allowField(true)->save($data);

        if($res){
            mailto($data['email'],'账户注册成功','注册管理员账户成功！');
            return 1;
        }else{
            return '注册失败！';
        }
    }
    //重置密码
    public function reset($data)
    {
        $validate = new \app\common\validate\Admin();
        if(!$validate->scene('reset')->check($data)){
            return $validate->getError();
        }
        if($data['code'] != session('code')){
            return '验证码不正确！';
        }
        $adminInfo = $this->where('email',$data['email'])->find();
        var_dump($data['email']);
        $password = mt_rand(10000,99999);
        $adminInfo->password = $password;
        $res = $adminInfo->save();
        if($res){
            $content = '恭喜你，密码重置成功！<br>用户名：'.$adminInfo['username'].'<br>新密码：'.$password;
            mailto($data['email'],'密码重置成功',$content);
            return 1;
        }else{
            return '密码重置失败！';
        }
    }
}
