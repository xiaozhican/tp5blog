<?php

namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class Article extends Model
{
    use SoftDelete;
    //关联栏目
    public function cate()
    {
        return $this->belongsTo('Cate','cate_id','id');
    }
    //文章添加
    public function add($data){
        $validate = new \app\common\validate\Article();
        if(!$validate->scene('add')->check($data)){
            return $validate->getError();
        }
        $res = $this->allowField(true)->save($data);
        if($res){
            return 1;
        }else{
            return '文章添加失败！';
        }
    }
    //推荐操作
    public function top($data)
    {
        $validate = new \app\common\validate\Article();
        if(!$validate->scene('top')->check($data)){
            return $validate->getError();
        }
        $articleInfo = $this->find($data['id']);
        $articleInfo->is_top = $data['is_top'];
        $res = $articleInfo->save();
        if($res){
            return 1;
        }else{
            return '操作失败！';
        }
    }
    //文章编辑
    public function edit($data)
    {
        $validate = new \app\common\validate\Article();
        if(!$validate->scene('edit')->check($data)){
            return $validate->getError();
        }
        $articleInfo = $this->find($data['id']);
        $articleInfo->title = $data['title'];
        $articleInfo->tags = $data['tags'];
        $articleInfo->is_top = $data['is_top'];
        $articleInfo->cate_id = $data['cate_id'];
        $articleInfo->desc = $data['desc'];
        $articleInfo->content = $data['content'];
        $res = $articleInfo->save();
        if($res){
            return 1;
        }else{
            return '文章编辑失败！';
        }
    }
}
