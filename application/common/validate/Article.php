<?php


namespace app\common\validate;


use think\Validate;

class Article extends Validate
{
    protected $rule = [
        'title|文章标题'=>'require|unique:article',
        'tags|文章标签'=>'require',
        'cate_id|所属栏目'=>'require',
        'desc|文章描述'=>'require',
        'content|文章内容'=>'require'
    ];
    //文章添加
    public function sceneAdd(){
        return $this->only(['title','tags','cate_id','desc','content']);
    }
    //推荐操作
    public function sceneTop()
    {
        return $this->only(['is_top']);
    }
    //文章编辑
    public function sceneEdit()
    {
        return $this->only(['title','tags','cate_id','desc','content']);
    }
}