<?php

namespace app\admin\controller;

use think\Controller;

class Cate extends Base
{
    //栏目列表
    public function list()
    {
        $cates = model('Cate')->order('sort')->paginate(10);
        $viewData = [
            'cates'=>$cates
        ];
        $this->assign($viewData);
        return view();
    }
    //栏目添加
    public function add(){
        if(request()->isAjax()){
            $data = [
                'catename'=>input('post.catename'),
                'sort'=>input('post.sort'),
            ];
            $res = model('Cate')->add($data);
            if($res == 1){
                $this->success('栏目添加成功','admin/cate/list');
            }else{
                $this->error($res);
            }
        }
        return view();
    }
    //排序
    public function sort()
    {
        $data = [
            'id'=>input('post.id'),
            'sort'=>input('post.sort'),
        ];
        $res = model('Cate')->sort($data);
        if($res==1){
            $this->success('排序成功！','admin/cate/list');
        }else{
            $this->error($res);
        }
    }
    //栏目编辑
    public function edit()
    {
        if(request()->isAjax()){
            $data = [
                'id'=>input('post.id'),
                'catename'=>input('post.catename')
            ];
            $res = model('Cate')->edit($data);
            if($res == 1){
                $this->success('栏目编辑成功！','admin/cate/list');
            }else{
                $this->error($res);
            }
        }
        $cateInfo = model('Cate')->find(input('get.id'));
        $viewData = [
            'cateInfo'=>$cateInfo
        ];
        $this->assign($viewData);
        return view();
    }
    //栏目删除
    public function del()
    {
        $cateInfo = model('Cate')->find(input('post.id'));
        $res = $cateInfo->delete();
        if($res){
            $this->success('栏目删除成功！','admin/cate/list');
        }else{
            $this->error('栏目删除失败！');
        }
    }
}
