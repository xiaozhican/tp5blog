<?php

namespace app\admin\controller;

use think\Controller;

class Article extends Base
{
    //文章列表
    public function list()
    {
        $articles = model('Article')->with('cate')->order(['is_top'=>'desc','update_time'=>'desc'])->paginate(10);
        $viewData = [
            'articles'=>$articles
        ];
        $this->assign($viewData);
        return view();
    }
    //文章添加
    public function add()
    {
        if(request()->isAjax()){
            $data = [
                'title'=>input('post.title'),
                'desc'=>input('post.desc'),
                'tags'=>input('post.tags'),
                'is_top'=>input('post.is_top',0),
                'cate_id'=>input('post.cate_id'),
                'content'=>input('post.content'),
            ];
            $res = model('Article')->add($data);
            if($res == 1){
                $this->success('文章添加成功！','admin/article/list');
            }else{
                $this->error($res);
            }
        }
        $cates = model('Cate')->select();
        $viewData = [
            'cates'=>$cates
        ];
        $this->assign($viewData);
        return view();
    }
    //推荐操作
    public function top()
    {
        if(request()->isAjax()){
            $data = [
                'id'=>input('post.id'),
                'is_top'=>input('psot.is_top')?0:1
            ];
            $res = model('Article')->top($data);
            if($res == 1){
                $this->success('操作成功！','admin/article/list');
            }else{
                $this->error($res);
            }
        }
    }
    //文章编辑
    public function edit()
    {
        if(request()->isAjax()){
            $data = [
                'id'=>input('post.id'),
                'title'=>input('post.title'),
                'desc'=>input('post.desc'),
                'tags'=>input('post.tags'),
                'is_top'=>input('post.is_top',0),
                'cate_id'=>input('post.cate_id'),
                'content'=>input('post.content'),
            ];
            $res = model('Article')->edit($data);
            if($res == 1){
                $this->success('文章编辑成功！','admin/article/list');
            }else{
                $this->error($res);
            }
        }
        $articleInfo = model('Article')->find(input('id'));
        $cates = model('Cate')->select();
        $viewData = [
            'articleInfo'=>$articleInfo,
            'cates'=>$cates
        ];
        $this->assign($viewData);
        return view();
    }
}
