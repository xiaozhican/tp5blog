<?php

namespace app\admin\controller;

use think\Controller;

class Index extends Controller
{
    //重复登录过滤
    public function initialize()
    {
        if(session('?admin.id')){
            $this->redirect('admin/home/index');
        }
    }

    //登录方法
    public function login()
    {
        if(request()->isAjax()){
            $data = [
                'username'=>input('post.username'),
                'password'=>input('post.password'),
            ];
            $res = model('Admin')->login($data);
            if($res == 1){
                $this->success('登录成功','admin/home/index');
            }else{
                $this->error($res);
            }
        }
        return view();
    }
    //注册
    public function register()
    {
        if(request()->isAjax()){
            $data = [
                'username'=>input('post.username'),
                'password'=>input('post.password'),
                'conpass'=>input('post.conpass'),
                'nickname'=>input('post.nickname'),
                'email'=>input('post.email'),
            ];
            $res = model('Admin')->register($data);
            //var_dump($res);die;
            if($res == 1){
                //var_dump(1111);
                $this->success('注册成功','admin/index/login');
            }else{
                $this->error($res);
            }
        }
        return view();
    }
    //忘记密码，发送验证码
    public function forget()
    {
        if(request()->isAjax()){
            //$memberInfo = model('Admin')->where('email',input('post.email'))->find();
            $code = mt_rand(1000,9999);
            session('code',$code);
//            $data = [
//                'email'=>input('post.email')
//            ];
            $res = mailto(input('post.email'),'重置密码验证码','您的重置密码验证码是：'.$code);
            if($res){
                //$this->success('验证码发送成功！');
                return json(['code'=>1,'msg'=>'验证码发送成功！']);
            }else{
                //$this->error('验证码发送失败！');
                return json(['code'=>0,'msg'=>'验证码发送失败！']);
            }
        }
        return view();
    }
    //重置密码
    public function reset()
    {
        //$code = input('post.code');
        $data = [
            'code'=>input('post.code'),
            'email'=>input('post.email')
        ];
        $res = model('Admin')->reset($data);
        if($res == 1){
            $this->success('密码重置成功,请重新登录','admin/index/login');
        }else{
            $this->error($res);
        }
    }
}
