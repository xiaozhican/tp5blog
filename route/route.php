<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::group('admin',function(){
    Route::rule('/','admin/index/login','get|post');//登录
    Route::rule('register','admin/index/register','get|post');//注册
    Route::rule('forget','admin/index/forget','get|post');//忘记密码
    Route::rule('reset','admin/index/reset','post');//重置密码
    Route::rule('index','admin/home/index','get');//后台首页
    Route::rule('logout','admin/home/logout','post');//退出
    Route::rule('catelist','admin/cate/list','get');//栏目列表
    Route::rule('cateadd','admin/cate/add','get|post');//栏目添加
    Route::rule('catesort','admin/cate/sort','post');//栏目排序
    Route::rule('cateedit/[:id]','admin/cate/edit','get|post');//栏目编辑
    Route::rule('catedel','admin/cate/del','post');//栏目删除
    Route::rule('articlelist','admin/article/list','get');//文章列表
    Route::rule('articleadd','admin/article/add','get|post');//文章添加
    Route::rule('articletop','admin/article/top','get|post');//文章推荐
    Route::rule('articleedit/[:id]','admin/article/edit','get|post');
});
