/*
Navicat MySQL Data Transfer

Source Server         : Laragon
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : tp5blog

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-03-06 10:40:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_admin
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin`;
CREATE TABLE `tp_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '管理员账户',
  `password` varchar(32) DEFAULT NULL COMMENT '管理员密码',
  `nickname` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '昵称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `status` enum('0','1') DEFAULT '0' COMMENT '状态，0是禁用，1是可用',
  `is_super` enum('1','0') DEFAULT '0' COMMENT '超级管理员',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '软删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tp_admin
-- ----------------------------
INSERT INTO `tp_admin` VALUES ('1', 'admin', 'admin', '管理员', 'aaa@qq.com', '1', '1', null, null, null);
INSERT INTO `tp_admin` VALUES ('2', 'admin11', '123456', 'aa', 'admin11@qq.com', '0', '0', '1582190297', '1582190297', null);
INSERT INTO `tp_admin` VALUES ('3', 'admin2', '123456', 'abc', '1073298557@qq.com', '0', '0', '1582191312', '1582191312', null);

-- ----------------------------
-- Table structure for tp_article
-- ----------------------------
DROP TABLE IF EXISTS `tp_article`;
CREATE TABLE `tp_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL COMMENT '文章标题',
  `desc` text COMMENT '概要',
  `tags` varchar(100) DEFAULT NULL COMMENT '标签',
  `content` longtext COMMENT '内容',
  `is_top` enum('1','0') CHARACTER SET latin1 DEFAULT '0' COMMENT '0是不推荐，1是推荐',
  `cate_id` int(11) DEFAULT NULL COMMENT '所属导航id',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of tp_article
-- ----------------------------
INSERT INTO `tp_article` VALUES ('1', '好好学习', '啊啊啊啊啊', 'php|赏金', '<p>好好学习</p>', '1', '1', '1582615637', '1582615637', null);
INSERT INTO `tp_article` VALUES ('2', 'php02', 'php02', 'php|赏金', '<p>php02</p>', '0', '1', '1582615741', '1582615741', null);
INSERT INTO `tp_article` VALUES ('3', 'thinkphp01', 'thinkphp01', 'thinkphp01', '<p>thinkphp01</p>', '1', '2', '1582615884', '1582615884', null);
INSERT INTO `tp_article` VALUES ('4', 'thinkphp020202', 'thinkphp02', 'thinkphp02', '<p>thinkphp020202</p>', '0', '2', '1582615908', '1582620499', null);

-- ----------------------------
-- Table structure for tp_cate
-- ----------------------------
DROP TABLE IF EXISTS `tp_cate`;
CREATE TABLE `tp_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catename` varchar(30) DEFAULT NULL COMMENT '导航名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='导航表';

-- ----------------------------
-- Records of tp_cate
-- ----------------------------
INSERT INTO `tp_cate` VALUES ('1', '我爱php', '4', '1582599458', '1582611799', null);
INSERT INTO `tp_cate` VALUES ('2', 'thinkphp', '2', '1582601273', '1582601273', null);

-- ----------------------------
-- Table structure for tp_comment
-- ----------------------------
DROP TABLE IF EXISTS `tp_comment`;
CREATE TABLE `tp_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text COMMENT '评论内容',
  `article_id` int(11) DEFAULT NULL COMMENT '评论id',
  `member_id` int(11) DEFAULT NULL COMMENT '评论用户',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='评论表';

-- ----------------------------
-- Records of tp_comment
-- ----------------------------

-- ----------------------------
-- Table structure for tp_member
-- ----------------------------
DROP TABLE IF EXISTS `tp_member`;
CREATE TABLE `tp_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='会员表';

-- ----------------------------
-- Records of tp_member
-- ----------------------------

-- ----------------------------
-- Table structure for tp_system
-- ----------------------------
DROP TABLE IF EXISTS `tp_system`;
CREATE TABLE `tp_system` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webname` varchar(50) DEFAULT NULL COMMENT '网站标题',
  `copyright` varchar(50) DEFAULT NULL COMMENT '版权信息',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='系统设置表';

-- ----------------------------
-- Records of tp_system
-- ----------------------------
